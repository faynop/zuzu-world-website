<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'spweb_zuzu');

/** MySQL database username */
define('DB_USER', 'spweb_zuzu');

/** MySQL database password */
define('DB_PASSWORD', '6p6S(374@x');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'fomin9mgh53r1cknnwpiu42ybmsaccs5qowxqjtyacfehoyqhrfnos5sgz1kn7qe');
define('SECURE_AUTH_KEY',  'unnpwcnrlo8s84n68gxk7tht9hqvvr41qn8rexywqzs2kbhx4mq1f8cmsfl19hff');
define('LOGGED_IN_KEY',    'qwgr9cmfuhu82sxdli1skxt8zmunpnbsvfrqnoeen8uyajxeidmskohfim6uyowl');
define('NONCE_KEY',        'aqkdwfchg5hdz7o3folgwdhrxhnkk5duai9cgkvbe6qbvr85ci6jw8a8hpemq4c2');
define('AUTH_SALT',        '1hfqsbelxtihp4rfkvwlsgoxydaq1pjvn4qncfwhrb9donpwtnpabttyspz2hkei');
define('SECURE_AUTH_SALT', 'bc7ft470ro74oxeucjpvj5qh1ie22ywuztizca4xqmr2jgux40ro4wzvpxwnindc');
define('LOGGED_IN_SALT',   '1gkmv1ezpraokp1kektzvroltav7y95lrfje0ftvzj78x0d6m1dr5ybdl3mpkgi4');
define('NONCE_SALT',       'aa4r6qr99ckwl2n5obrlxrgkibefgbizsnmz27gyayiaqmw0g5pp1vlyqrytuc42');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpst_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
